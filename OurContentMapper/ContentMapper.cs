﻿using System;
using System.Collections.Generic;
using Antonio.ContentMapper.Data;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.Persistence;
using Umbraco.Web;
using ContentType = Antonio.ContentMapper.Data.ContentType;

namespace Antonio.ContentMapper
{
    public sealed class ContentMapper
    {
        #region Singleton

        private static readonly Lazy<ContentMapper> Lazy =
            new Lazy<ContentMapper>(() => new ContentMapper());
    
        public static ContentMapper Current => Lazy.Value;

        private ContentMapper()
        {
        }

        #endregion

        /// <summary>
        /// Get the entire list of all Content Maps currently stored
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ContentMap> GetMap()
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;

            return db.Fetch<ContentMap>(new Sql().Select("*").From(ContentMap.TableName));
        }

        /// <summary>
        /// Get a specific Content Map by database id
        /// </summary>
        /// <param name="dbId">Database id</param>
        /// <returns>Map or Default</returns>
        public ContentMap GetMap(int dbId)
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;
            var result = db.FirstOrDefault<ContentMap>(new Sql().Select("*").From(ContentMap.TableName).Where("Id = @0", dbId));
           
            return result;
        }

        /// <summary>
        /// Delete a Content map by database id
        /// </summary>
        /// <param name="dbId">Database id</param>
        /// <returns>true on success</returns>
        public bool Delete(int dbId)
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;
            return db.Delete<ContentMap>(dbId) == 1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key">Content map key (guid as string)</param>
        /// <returns></returns>
        public IPublishedContent GetContent(string key)
        {
            return GetContent(new Guid(key));
        }

        /// <summary>
        /// Get the umbrack IPublished content for a given Content Map key.<br />
        /// regardless if its media or content
        /// </summary>
        /// <param name="key">Content Map key</param>
        /// <returns></returns>
        public IPublishedContent GetContent(Guid key)
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;

            var data = db.FirstOrDefault<ContentMap>(new Sql().Select("*").From(ContentMap.TableName).Where("MapKey = @0", key));

            if (data == null) return null;

            var helper = new UmbracoHelper(UmbracoContext.Current);

            switch (data.Type)
            {
                case ContentType.Media:
                    return data.ContentId >= 0 ? helper.TypedMedia(data.ContentId) : helper.TypedMedia(data.ContentGuid);
                case ContentType.Content:
                default:
                    return data.ContentId >= 0 ? helper.TypedContent(data.ContentId) : helper.TypedContent(data.ContentGuid);
            }
        }

        /// <summary>
        /// Save or Update a Content Map
        /// </summary>
        /// <param name="contentMap"></param>
        /// <returns></returns>
        public ContentMap SaveContentMap(ContentMap contentMap)
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;

            //truncate description - length is 5000
            if (!string.IsNullOrEmpty(contentMap.Description) && contentMap.Description.Length > 5000)
            {
                LogHelper.Warn<ContentMapper>($"Truncating description. Max length is 5000. {contentMap}");
                contentMap.Description = contentMap.Description.Substring(0, 5000);
            }

            try
            {
                if (contentMap.Id > 0)
                {
                    //update
                    db.Update(contentMap);
                }
                else
                {
                    //create
                    db.Insert(contentMap);
                }
            }
            catch (Exception e)
            {
                LogHelper.Error<ContentMapper>("Error saving content map",e);
                throw;
            }

            return contentMap;
        }
    }
}