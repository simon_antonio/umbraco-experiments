﻿using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.Migrations;
using Umbraco.Core.Persistence.SqlSyntax;

namespace Antonio.ContentMapper.Data.Migrations
{
    [Migration("1.0.0", 1,  ContentMap.Name)]
    public class ContentMapMigration_1_0_0 : MigrationBase
    {
        private readonly UmbracoDatabase _database = ApplicationContext.Current.DatabaseContext.Database;
        private readonly DatabaseSchemaHelper _schemaHelper;

        public ContentMapMigration_1_0_0(ISqlSyntaxProvider sqlSyntax, ILogger logger)
            : base(sqlSyntax, logger)
        {
            _schemaHelper = new DatabaseSchemaHelper(_database, logger, sqlSyntax);
        }

        public override void Up()
        {
            if (!_schemaHelper.TableExist(ContentMap.TableName))
            {
                _schemaHelper.CreateTable<ContentMap>(false);
            }

            Alter.Table(ContentMap.TableName).AlterColumn("Description").AsString(4000);
            //db.Execute("ALTER TABLE Test ALTER COLUMN Description NVARCHAR(MAX)");
        }

        public override void Down()
        {
            _schemaHelper.DropTable<ContentMap>();
        }
    }
}