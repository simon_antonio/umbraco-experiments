﻿using System;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace Antonio.ContentMapper.Data
{
    public enum ContentType
    {
        Content,
        Media,
        Other
    }

    [TableName(TableName)]
    [PrimaryKey("Id", autoIncrement =true)]
    public class ContentMap
    {
        public const string Name = "ContentMap";
        public const string TableName = "cmContentMap";

        [Column("Id")]
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }

        [Column("MapKey")]
        public Guid MapKey { get; set; }

        [Column("ContentGuid")]
        [NullSetting(NullSetting = NullSettings.Null)]
        public Guid? ContentGuid { get; set; }

        [Column("ContentId")]
        [NullSetting(NullSetting = NullSettings.Null)]
        public int? ContentId { get; set; }
        
        [Column("ContentTypeId")]
        public int _ContentTypeId { get; set; }

        [Column("Description")]
        public string Description { get; set; }

        [Ignore]
        public ContentType Type
        {
            get { return (ContentType) _ContentTypeId; }
            set { _ContentTypeId = (int)value; }
        }

        [Column("CreatedDate")]
        [Constraint(Default = "getdate()")]
        public DateTime Created { get; set; }

        public ContentMap()
        {
            Created = DateTime.Now;
        }

        public override string ToString()
        {
            return $"({Id}) {MapKey} to {ContentGuid}/{ContentId} for {Description} on {Created}";
        }
    }
}