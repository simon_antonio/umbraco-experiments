﻿angular.module("umbraco").controller("Antonio.ContentMapper", [
    "$scope",
    "$rootScope",
    "$timeout",
    "$routeParams",
    "editorState",
    "assetsService",
    "Antonio.ContentMapper.Resources",
    "umbRequestHelper",
    "notificationsService",
    "dialogService",

    function ($scope, $rootScope, $timeout, $routeParams, editorState, assetsService, ourResources, umbRequestHelper, notificationsService, dialogService) {

        $scope.isDebug = false;
        $scope.contentMapList = [];
        $scope.newRow = {};

        //todo - read from umb value
        //todo - editing

        $scope.addMapping = function () {
            //todo highlight error fields
            console.log("addMapping", $scope.newRow);

            var contentId = 0;
            if ($scope.newRow.content.udi != undefined && $scope.newRow.content.udi != null) {
                contentId = $scope.newRow.content.udi;
            } else if ($scope.newRow.content.nodeId != undefined && $scope.newRow.content.nodeId != null) {
                contentId = $scope.newRow.content.nodeId;
            } else {
                notificationsService.warning("Missing content target",
                    "Make sure you select a valid content node to map");
                return;
            }

            if (!isGuid($scope.newRow.key)) {
                notificationsService.warning("Invalid mapping key",
                    "Make sure you use a valid guid for your mapping key");
                return;
            }

            ourResources.addMap($scope.newRow.key, contentId, $scope.newRow.description).then((e) => {
                $scope.newRow = newRow();
                refreshData();
            });
        };

        $scope.showContentPicker = function () {
            dialogService.contentPicker({
                callback: function (item) {
                    console.log("picked", item);
                    $scope.newRow.content = {
                        nodeId: item.id,
                        udi: item.metaData.UniqueID,
                        contentType: "content",
                        name: item.name
                    }
                }
            });
        };
        $scope.showMediaPicker = function () {
            dialogService.mediaPicker({
                callback: function (item) {
                    console.log("picked", item);
                    $scope.newRow.content = {
                        nodeId: item.id,
                        udi: item.key,
                        contentType: "media",
                        name: item.name
                    }
                }
            });
        };

        $scope.deleteMapping = function (id) {
            ourResources.deleteMap(id).then((e) => {
                refreshData();
            });
        }

        function guid() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        }

        function isGuid(input) {
            return new RegExp("^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$", "i").test(input.toString());
        }

        //new row object
        function newRow() {
            return {
                id: null,
                key: guid(),
                content: {
                    nodeId: null,
                    udi: null,
                    contentType: null,
                    name: null
                },
                contentId: null,
                description: ""
            };
        }

        function refreshData() {
            ourResources.getMaps().then((d) => {
                console.log("getMap", d);
                $scope.contentMapList = d;
            });
        }

        $scope.init = function () {
            refreshData();

            //new row binding
            $scope.newRow = newRow();
        }
        $scope.init();
    }
]);

angular.module('umbraco.resources').factory('Antonio.ContentMapper.Resources',
    function ($q, $http, umbRequestHelper) {
        return {
            getMaps: function () {
                var url = umbRequestHelper.convertVirtualToAbsolutePath("~/umbraco/backoffice/AntonioContentMapper/Api/GetAllMaps");
                return umbRequestHelper.resourcePromise(
                    $http.get(url),
                    'Failed to get all content maps'
                );
            },
            addMap: function (key, contentId, description) {
                //id = db id OR 0, key = your key, contentId = nodeId or node UID, desc is your notes
                var url = umbRequestHelper.convertVirtualToAbsolutePath("~/umbraco/backoffice/AntonioContentMapper/Api/AddMap?key=" + key + "&nodeId=" + contentId + "&description=" + description);
                return umbRequestHelper.resourcePromise(
                    $http.get(url),
                    'Failed to update map ' + key
                );
            },
            deleteMap: function (id) {
                var url = umbRequestHelper.convertVirtualToAbsolutePath("~/umbraco/backoffice/AntonioContentMapper/Api/DeleteMap?id=" + id);
                return umbRequestHelper.resourcePromise(
                    $http.get(url),
                    'Failed to update map ' + id
                );
            }
        };
    });