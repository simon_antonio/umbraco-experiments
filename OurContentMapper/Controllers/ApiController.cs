﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Antonio.ContentMapper.Data;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Editors;
using Umbraco.Web.Mvc;
using ContentType = Antonio.ContentMapper.Data.ContentType;

namespace Antonio.ContentMapper.Controllers
{
    [PluginController("AntonioContentMapper")]
    public class ApiController : UmbracoAuthorizedJsonController
    {
        /// <summary>
        /// /umbraco/backoffice/AntonioContentMapper/Api/GetAllMaps
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        public IEnumerable<object> GetAllMaps()
        {
            return ContentMapper.Current.GetMap().Select(x => new Map()
            {
                id = x.Id,
                key = x.MapKey,
                contentId = x.ContentId,
                description = x.Description,
                created = x.Created,
                content = new MapContent()
                {
                    nodeId = x.ContentId,
                    udi = x.ContentGuid,
                    contentType = x.Type == ContentType.Content ? "content" : "media",
                    name = x.Type == ContentType.Content ? Umbraco.TypedContent(x.ContentGuid).Name : Umbraco.TypedMedia(x.ContentGuid).Name,
                    path = x.Type == ContentType.Content ? Umbraco.TypedContent(x.ContentGuid).Path : Umbraco.TypedMedia(x.ContentGuid).Path
                }
            });
        }

        /// <summary>
        /// /umbraco/backoffice/AntonioContentMapper/Api/AddMap?id={0}&key={1}&nodeId={2}&description={3}
        /// </summary>
        /// <param name="id"></param>
        /// <param name="key"></param>
        /// <param name="nodeId"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        public bool AddMap([FromUri] Guid key, [FromUri] string nodeId, [FromUri] string description)
        {
            //todo - sloppy re-work this

            IPublishedContent content = null;
            bool isMedia = false;

            if (Guid.TryParse(nodeId, out var lGuid))
            {
                var t = Umbraco.TypedContent(lGuid);
                if (t == null)
                {
                    t = Umbraco.TypedMedia(lGuid);
                    isMedia = true;
                }
                content = t;
            }
            else if (int.TryParse(nodeId, out var lid))
            {
                var t = Umbraco.TypedContent(lid);
                if (t == null)
                {
                    t = Umbraco.TypedMedia(lid);
                    isMedia = true;
                }
                content = t;
            }

            if (content == null) return false;

            var map = new ContentMap()
            {
                ContentGuid = lGuid,
                ContentId = content.Id,
                MapKey = key,
                Description = description?.Trim() ?? content.Name,
                Type = (isMedia ? ContentType.Media : ContentType.Content)
            };

            var saved = ContentMapper.Current.SaveContentMap(map);
            return (saved?.Id > 0);
        }

        /// <summary>
        /// /umbraco/backoffice/AntonioContentMapper/Api/Delete?id={0}
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        public bool DeleteMap([FromUri] int id)
        {
            return ContentMapper.Current.Delete(id);
        }

        #region Json Model
        private class Map
        {
            public int id { get; set; }
            public Guid key { get; set; }
            public MapContent content { get; set; }
            public int? contentId { get; set; }
            public string description { get; set; }
            public DateTime created { get; set; }
        }
        private class MapContent
        {
            public int? nodeId { get; set; }
            public Guid? udi { get; set; }
            public string contentType { get; set; }
            public string name { get; set; }
            public string path { get; set; }
        }
        #endregion
    }
}
