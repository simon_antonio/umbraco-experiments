<?xml version="1.0" encoding="utf-8" ?>
<Project xmlns="http://schemas.microsoft.com/developer/msbuild/2003" DefaultTargets="Package">
	
	<!-- IMPORTS -->
	<PropertyGroup>
		<MSBuildCommunityTasksPath>$(MSBuildProjectDirectory)\Tools\MSBuildCommunityTasks</MSBuildCommunityTasksPath>
		<MSBuildUmbracoTasksPath>$(MSBuildProjectDirectory)\Tools\MSBuildUmbracoTasks</MSBuildUmbracoTasksPath>
		<MSBuildNugetTasksPath>$(MSBuildProjectDirectory)\Tools\MSBuildNugetTasks</MSBuildNugetTasksPath>
		<AppVeyorUmbracoPath>$(MSBuildProjectDirectory)\Tools\AppVeyorUmbraco</AppVeyorUmbracoPath>
	</PropertyGroup>

	<Import Project="$(MSBuildCommunityTasksPath)\MSBuild.Community.Tasks.Targets" />
	<Import Project="$(MSBuildUmbracoTasksPath)\MSBuild.Umbraco.Tasks.Targets" />
	<Import Project="$(MSBuildNugetTasksPath)\MSBuild.NuGet.Tasks.Targets" />
	<Import Project="$(AppVeyorUmbracoPath)\AppVeyorUmbraco.Targets" />
	
	 <!-- SHARED PROPERTIES -->
	 <PropertyGroup>
		<PackageName>Content Mapper</PackageName>
		<MinUmbracoVersion>7.4.3</MinUmbracoVersion>
		<Readme><![CDATA[This package implements a content mapping system, for different environments (development, staging, production) so the user does not have to use umbraco node ids]]></Readme>
		<AuthorName>Simon Antonio</AuthorName>
		<AuthorUrl>https://nulldragon.net/</AuthorUrl>
		<PackageLicenseName></PackageLicenseName>
		<PackageLicenseUrl></PackageLicenseUrl>
		<ProjectUrl></ProjectUrl>
	 </PropertyGroup>
	 
	 <!-- NUGET ONLY PROPERTIES -->
	  <PropertyGroup>
		<PackageId>Antonio.ContentMapper</PackageId>		
		<Copyright>Copyright &#169; Simon Antonio and contributors.</Copyright>
		<Owners>Simon Antonio</Owners>
		<Description>This package implements a content mapping system, for different environments (development, staging, production) so the user does not have to use umbraco node ids</Description>
		<IconUrl></IconUrl>
		<Tags>umbraco</Tags>
		<Language>en-GB</Language>
		<RequireLicenseAcceptance>false</RequireLicenseAcceptance>
	  </PropertyGroup>
	  
	<!-- APPVEYOR PROPERTIES -->
	  <PropertyGroup>
		<FileVersion>$(APPVEYOR_BUILD_VERSION)</FileVersion>
	  </PropertyGroup>
	  <Choose>
		<When Condition="$(APPVEYOR_BUILD_NUMBER) != '' And $(APPVEYOR_REPO_TAG) != 'true' ">
		  <PropertyGroup>
			<Release>false</Release>
		  </PropertyGroup>
		</When>
		<Otherwise>
		  <PropertyGroup>
			<Release>true</Release>
		  </PropertyGroup>
		</Otherwise>
	  </Choose>

    <!-- PATHS -->	  
	<PropertyGroup>
		<BuildConfig>Release</BuildConfig>
		<RootDir>$(MSBuildProjectDirectory)\..</RootDir>
		<BuildUmbDir>$(MSBuildProjectDirectory)\UmbracoBuild</BuildUmbDir>
		<BuildNuGetDir>$(MSBuildProjectDirectory)\NugetBuild</BuildNuGetDir>
		<ArtifactsDir>$(RootDir)\artifacts</ArtifactsDir>
		<CoreProjectDir>$(RootDir)\OurContentMapper\</CoreProjectDir>		
		<AppPluginDir>$(CoreProjectDir)\App_Plugins</AppPluginDir>
	</PropertyGroup>
	
	<!-- PROJECTS TO BUILD -->
	<ItemGroup>
		<ProjectToBuild Include="$(CoreProjectDir)\OurContentMapper.csproj" >
			<Properties>Configuration=Release</Properties>
		</ProjectToBuild>		
	</ItemGroup>
	
	<!-- TARGETS -->
	  <Target Name="GetProductVersion">		
		<GetProductVersion BuildVersion="$(APPVEYOR_BUILD_VERSION)" BuildSuffix="$(UMBRACO_PACKAGE_PRERELEASE_SUFFIX)" Release="$(Release)">
		  <Output TaskParameter="ProductVersion" PropertyName="ProductVersion"/>
		</GetProductVersion>
	  </Target>
	  
	<!-- CLEAN -->
	  <Target Name="Clean" DependsOnTargets="GetProductVersion">
		<RemoveDir Directories="$(BuildUmbDir)" Condition="Exists('$(BuildUmbDir)')" />
		<RemoveDir Directories="$(BuildNuGetDir)" Condition="Exists('$(BuildNuGetDir)')" />
		<RemoveDir Directories="$(ArtifactsDir)" Condition="Exists('$(ArtifactsDir)')" />		
		<MakeDir Directories="$(BuildUmbDir)" />
		<MakeDir Directories="$(BuildNuGetDir)" />		
		<MakeDir Directories="$(ArtifactsDir)" />
	  </Target>
	  
	<!-- UPDATE PROJECT ASSEMBLEY VERSION -->
	  <Target Name="UpdateAssemblyInfo" DependsOnTargets="Clean">
		  <FileUpdate Encoding="ASCII" Files="$(CoreProjectDir)\Properties\AssemblyInfo.cs" Regex="AssemblyVersion\(&quot;.*&quot;\)\]" ReplacementText="AssemblyVersion(&quot;$(FileVersion)&quot;)]" />
		  <FileUpdate Encoding="ASCII" Files="$(CoreProjectDir)\Properties\AssemblyInfo.cs" Regex="AssemblyFileVersion\(&quot;.*&quot;\)\]" ReplacementText="AssemblyFileVersion(&quot;$(FileVersion)&quot;)]" />
		  <FileUpdate Encoding="ASCII" Files="$(CoreProjectDir)\Properties\AssemblyInfo.cs" Regex="AssemblyInformationalVersion\(&quot;.*&quot;\)\]" ReplacementText="AssemblyInformationalVersion(&quot;$(ProductVersion)&quot;)]" />		   
	  </Target>
	  
	  <!-- COMPILE -->
	  <Target Name="Compile" DependsOnTargets="UpdateAssemblyInfo">
		<MSBuild Projects="@(ProjectToBuild)" Properties="Configuration=$(BuildConfig)"/>
	  </Target>
	  
	    <!-- PREPARE FILES -->
  <Target Name="PrepareFiles" DependsOnTargets="Compile">
    <ItemGroup>
      <!-- Shared -->
      <BinFiles Include="$(CoreProjectDir)\bin\$(BuildConfig)\AntonioContentMapper.dll" />
      <PdbFiles Include="$(CoreProjectDir)\bin\$(BuildConfig)\AntonioContentMapper.pdb" />
	 
      <!-- Umbraco specific -->
      <PackageFile Include="$(MSBuildProjectDirectory)\package.xml" />      

      <!-- NuGet specific -->
      <NuSpecFile Include="$(MSBuildProjectDirectory)\package.nuspec" />

			<!-- App Plugins -->
			<AppPluginFiles Include="$(AppPluginDir)\**\*" />
    </ItemGroup>

	  <!-- Umbraco -->
	  <Copy SourceFiles="@(BinFiles)" DestinationFolder="$(BuildUmbDir)\bin" />
	  <Copy SourceFiles="@(PackageFile)" DestinationFolder="$(BuildUmbDir)" />
	  

		<!--NuGet -->
	  <Copy SourceFiles="@(BinFiles)" DestinationFolder="$(BuildNuGetDir)\lib\net45" />
	  <Copy SourceFiles="@(PdbFiles)" DestinationFolder="$(BuildNuGetDir)\lib\net45" />	 
	  <Copy SourceFiles="@(NuSpecFile)" DestinationFolder="$(BuildNuGetDir)" />

		<!-- App_Plugin -->
		<Copy SourceFiles="@(AppPluginFiles)" DestinationFiles="@(AppPluginFiles->'$(BuildUmbDir)\App_Plugins\%(RecursiveDir)%(Filename)%(Extension)')" />
    <Copy SourceFiles="@(AppPluginFiles)" DestinationFiles="@(AppPluginFiles->'$(BuildNuGetDir)\content\App_Plugins\%(RecursiveDir)%(Filename)%(Extension)')" />
  </Target>
  
  <!-- MANIFEST UMBRACO -->
  <Target Name="ManifestUmbraco" DependsOnTargets="PrepareFiles">
    <ItemGroup>
      <ManifestFiles Include="$(BuildUmbDir)\**\*" Exclude="$(BuildUmbDir)\package.xml" />
    </ItemGroup>
    <ManifestUpdate
      ManifestFile="$(BuildUmbDir)\package.xml"
      WorkingDirectory="$(BuildUmbDir)"
      MinimumRequiredUmbracoVersion="$(MinUmbracoVersion)"
      PackageName="$(PackageName)"
      PackageVersion="$(ProductVersion)"
      AuthorName="$(AuthorName)"
      AuthorUrl="$(AuthorUrl)"
      Readme="$(Readme)"
      PackageLicenseName="$(PackageLicenseName)"
      PackageLicenseUrl="$(PackageLicenseUrl)"
      PackageUrl="$(ProjectUrl)"
      Files="@(ManifestFiles)"
      IconUrl="$(IconUrl)"/>
  </Target>
  
  <!-- MANIFEST FOR NUGET PACKAGE -->
  <Target Name="ManifestNuGet" DependsOnTargets="PrepareFiles">
    <ItemGroup>
      <ManifestFiles Include="$(BuildNuGetDir)\**\*" Exclude="$(BuildNuGetDir)\package.nuspec" />
    </ItemGroup>
    <MSBuild.NuGet.Tasks.ManifestUpdate
      ManifestFile="$(BuildNuGetDir)\package.nuspec"
      WorkingDirectory="$(BuildNuGetDir)"
      Title="$(PackageName)"
      Description="$(Description)"
      Summary="$(Readme)"
      Version="$(ProductVersion)"
      MinimumRequiredUmbracoVersion ="$(MinUmbracoVersion)"
      Authors="$(AuthorName)"
      Owners="$(Owners)"
      Copyright="$(Copyright)"
      LicenseUrl="$(PackageLicenseUrl)"
      ProjectUrl="$(ProjectUrl)"
      Id="$(PackageId)"
      IconUrl="$(IconUrl)"
      Language="$(Language)"
      RequireLicenseAcceptance="$(RequireLicenseAcceptance)"
      Tags="$(Tags)"
      Files="@(ManifestFiles)" />
  </Target>
  
  <!-- PACKAGE -->
  <Target Name="Package" DependsOnTargets="ManifestUmbraco; ManifestNuGet;">
    <ItemGroup>
      <PackageFiles Include="$(BuildUmbDir)\**\*.*" />
    </ItemGroup>

    <Package ManifestFile="$(BuildUmbDir)\package.xml"
      WorkingDirectory="$(BuildUmbDir)"
      OutputDirectory="$(ArtifactsDir)"
      Files="@(PackageFiles)" />

    <MSBuild.NuGet.Tasks.Pack NuGetExePath="$(RootDir)\Packaging\tools\NuGet.exe"
      ManifestFile="$(BuildNuGetDir)\package.nuspec"
      BasePath="$(BuildNuGetDir)"
      Version="$(ProductVersion)"
      OutputDirectory="$(ArtifactsDir)"
      Symbols="false" />    

  </Target>
	
</Project>