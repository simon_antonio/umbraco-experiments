[
    {
        "name": "Rich text editor",
        "alias": "rte",
        "view": "rte",
        "icon": "icon-article"
    },
    {
        "name": "Image",
        "alias": "media",
        "view": "media",
        "icon": "icon-picture"
    },
    {
        "name": "Macro",
        "alias": "macro",
        "view": "macro",
        "icon": "icon-settings-alt"
    },
    {
        "name": "Embed",
        "alias": "embed",
        "view": "embed",
        "icon": "icon-movie-alt"
    },
    {
        "name": "Headline",
        "alias": "headline",
        "view": "textstring",
        "icon": "icon-coin",
        "config": {
            "style": "font-size: 36px; line-height: 45px; font-weight: bold",
            "markup": "<h1>#value#</h1>"
        }
    },
    {
        "name": "Quote",
        "alias": "quote",
        "view": "textstring",
        "icon": "icon-quote",
        "config": {
            "style": "border-left: 3px solid #ccc; padding: 10px; color: #ccc; font-family: serif; font-style: italic; font-size: 18px",
            "markup": "<blockquote>#value#</blockquote>"
        }
    },
    {
        "name": "Banner",
        "alias": "banner",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-umb-media ",
        "config": {
            "allowedDocTypes": [
                "^banner$"
            ],
            "nameTemplate": "",
            "enablePreview": true,
            "viewPath": "/Views/Grid/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Blog Search",
        "alias": "blogSearch",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-tags ",
        "config": {
            "allowedDocTypes": [
                "^blogSearch$"
            ],
            "nameTemplate": "",
            "enablePreview": true,
            "viewPath": "/Views/Grid/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Card",
        "alias": "card",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-panel-show",
        "config": {
            "allowedDocTypes": [
                "^card$"
            ],
            "nameTemplate": "",
            "enablePreview": true,
            "viewPath": "/Views/Grid/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Card List",
        "alias": "cardList",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-thumbnail-list",
        "config": {
            "allowedDocTypes": [
                "^cardList$"
                ],
            "nameTemplate": "",
            "enablePreview": true,
            "viewPath": "/Views/Grid/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Child Page List",
        "alias": "childPageList",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-item-arrangement",
        "config": {
            "allowedDocTypes": [
                "^childPageList$"
            ],
            "nameTemplate": "",
            "enablePreview": true,
            "viewPath": "/Views/Grid/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Download Link",
        "alias": "downloadLink",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-download-alt",
        "config": {
            "allowedDocTypes": [
                "^downloadLink$"
            ],
            "nameTemplate": "",
            "enablePreview": true,
            "viewPath": "/Views/Grid/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Event Search",
        "alias": "eventSearch",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-cinema",
        "config": {
            "allowedDocTypes": [
                "^eventSearch$"
            ],
            "nameTemplate": "",
            "enablePreview": true,
            "viewPath": "/Views/Grid/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Image Gallery",
        "alias": "imageGallery",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-camera-roll",
        "config": {
            "allowedDocTypes": [
                "^imageGallery$"
            ],
            "nameTemplate": "",
            "enablePreview": true,
            "viewPath": "/Views/Grid/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Latest Blog Posts",
        "alias": "latestBlogPosts",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-post-it",
        "config": {
            "allowedDocTypes": [
                "^latestBlogPosts$"
            ],
            "nameTemplate": "",
            "enablePreview": true,
            "viewPath": "/Views/Grid/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Latest Events",
        "alias": "latestEvents",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-calendar-alt",
        "config": {
            "allowedDocTypes": [
                "^latestEvents$"
            ],
            "nameTemplate": "",
            "enablePreview": true,
            "viewPath": "/Views/Grid/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Latest News",
        "alias": "latestNews",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-newspaper-alt",
        "config": {
            "allowedDocTypes": [
                "^latestNews$"
            ],
            "nameTemplate": "",
            "enablePreview": true,
            "viewPath": "/Views/Grid/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Line Graph",
        "alias": "lineGraph",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-chart",
        "config": {
            "allowedDocTypes": [
                "^lineGraph$"
            ],
            "nameTemplate": "",
            "enablePreview": true,
            "viewPath": "/Views/Grid/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Simple Map",
        "alias": "simpleMap",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-map-location",
        "config": {
            "allowedDocTypes": [
                "^simpleMap$"
            ],
            "nameTemplate": "",
            "enablePreview": true,
            "viewPath": "/Views/Grid/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "News Search",
        "alias": "newsSearch",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-newspaper",
        "config": {
            "allowedDocTypes": [
                "^newsSearch$"
            ],
            "nameTemplate": "",
            "enablePreview": true,
            "viewPath": "/Views/Grid/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Search",
        "alias": "search",
        "view": "/App_Plugins/StaticGridContent/StaticGridContent.html",
        "render": "/App_Plugins/StaticGridContent/StaticGridContentRender.html",
        "icon": "icon-search",
        "config": {
            "viewName": "Search",
        }
    },
    {
        "name": "Sibling Page List",
        "alias": "siblingPageList",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-user-females-alt",
        "config": {
            "allowedDocTypes": [
                "^siblingPageList$"
            ],
            "nameTemplate": "",
            "enablePreview": true,
            "viewPath": "/Views/Grid/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Site Map",
        "alias": "siteMap",
        "view": "/App_Plugins/StaticGridContent/StaticGridContent.html",
        "render": "/App_Plugins/StaticGridContent/StaticGridContentRender.html",
        "icon": "icon-sitemap",
        "config": {
            "viewName": "SiteMap",
            "controllerName": "SiteMap"
        }
    },
    {
        "name": "Sample Html Elements",
        "alias": "sampleElements",
        "view": "/App_Plugins/StaticGridContent/StaticGridContent.html",
        "render": "/App_Plugins/StaticGridContent/StaticGridContentRender.html",
        "icon": "icon-wrong",
        "config": {
            "viewName": "SampleElements"
        }
    },
]