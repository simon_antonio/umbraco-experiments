﻿
[NPM](https://nodejs.org/en/) & [webpack](https://webpack.js.org/) notes

Run from ~/Project/ directory

h3. New setup run the following
npm init -y
npm install webpack webpack-cli --save-dev

h3. To install packages make sure you run npm from the ~/Project/Resources/ directory

h3. To use webpack-hot-reload
hit run in visual studio
wait for the project to load

open power shell, or vs code, or the package manager console and run


npm run webpack

this will start the webpack-dev-server
take note of the port it outputs it will be different from the port you entered.
view project via this other port.

h3. Global npm installs
npm install -g eslint

make sure to use the external version of npm if in visual studio.