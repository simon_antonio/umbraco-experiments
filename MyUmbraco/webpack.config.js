﻿const webpack = require("webpack");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

module.exports = (env, argv) => {
    const isDebug = argv.mode !== 'production';
    return {
    watchOptions: {
        aggregateTimeout: 300,
        poll: 1000,
        ignored: /node_modules/
      },
    //what to package
    entry: {
        app: [
            './Resources/assets/scripts/site-controller.js',
            './Resources/assets/styles/main.scss'
        ],
        /*vendor:[
            './assets/scripts/vendor/*.js'            
        ]*/
    },
     //where to put our files after we are done
     output: {
         path: __dirname + '/Resources/bundles',
        filename: 'site.js',
        publicPath: '/Resources/bundles/'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules|bower_components)/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: { presets:['@babel/preset-react','@babel/preset-env']}
                    },
                    { loader: 'eslint-loader' }
                ]
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    isDebug ? { loader: 'style-loader' } : { loader: MiniCssExtractPlugin.loader }, //while debugging css served via javascript.
                    isDebug ? { loader: 'css-loader', options: { sourceMap: true} } : { loader: 'css-loader?minimize' },
                    {
                        loader: 'postcss-loader',
                        options: {
                          plugins: () => [
                            require('precss'),
                            require('autoprefixer'),
                          ]
                        }
                    },
                    isDebug ? { loader: 'sass-loader', options: { sourceMap: true}  } : { loader:'sass-loader' },
                ]
            }
        ]
    },
    //setup any plugins
    plugins:[
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css'
        }),
        new webpack.HotModuleReplacementPlugin()
    ],   
    optimization: {
        minimizer:[
            new UglifyJsPlugin({
                cache:true,
                parallel:true,
                sourceMap: isDebug
            }),
            new OptimizeCSSAssetsPlugin({})
        ]
    },
    devtool: isDebug ? 'source-map' : 'none',
    devServer: isDebug? {
        contentBase: './bundles',
        hot: true,
        proxy: {
            '*':{
                target:'http://localhost:59509/'
            }
        }
    } : {}
};
};