﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using Microsoft.Owin;
using MyUmbraco.Application.Services;
using MyUmbraco.Application.Services.Interfaces;
using Ninject.Modules;
using Ninject.Web.Common;
using umbraco.editorControls;
using Umbraco.Core;
using Umbraco.Core.Configuration;
using Umbraco.Core.Configuration.HealthChecks;
using Umbraco.Core.Models.Identity;
using Umbraco.Core.Security;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.HealthCheck;

namespace MyUmbraco.Application.IoC
{
    public class NinjectHttpModules
    {
        //Return Lists of Modules in the Application
        public static NinjectModule[] Modules => new NinjectModule[] { new MainModule() };

        //Main Module For Application
        public class MainModule : NinjectModule
        {
            public override void Load()
            {
                //@Info - Add your concrete bindings here

                //Bind<HttpContext>().ToMethod(c => HttpContext.Current);//this is a bit dirty but it works, allows us to inject HttpContext
                Bind<INewsContext>().To<NewsContent>();
                Bind<ICacheResolver>().To<CacheResolver>();
                Bind<ISiteConfiguration>().To<SiteConfiguration>();

                //Todo work out how to DI UmbracoContext, and ApplicationContext
                //Bind<Umbraco.Core.ApplicationContext>().To<ApplicationContext>();
                //Bind<IUmbracoService>().ToConstructor(ct => new UmbracoServices)
                
                //todo work our if we need - DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
                //Umbraco.Web registrations
                Bind<IOwinContext>().ToMethod(x => HttpContext.Current.GetOwinContext()).InRequestScope();//not sure about this one
                Bind<IHealthCheckResolver>().ToMethod(x => HealthCheckResolver.Current);
                Bind<UmbracoContext>().ToMethod(x => UmbracoContext.Current);
                Bind<UmbracoHelper>().ToMethod(x => new UmbracoHelper()).InRequestScope();//hopefully works
                Bind<BackOfficeUserManager<BackOfficeIdentityUser>>().ToMethod(x =>
                    HttpContext.Current.GetOwinContext().GetBackOfficeUserManager()).InTransientScope();//hopefully this works too

                //Umbraco.Core registrations
                Bind<ApplicationContext>().ToMethod(x => ApplicationContext.Current);//.InRequestScope();
                Bind<ServiceContext>().ToMethod(x => ApplicationContext.Current.Services);//.InRequestScope();
                Bind<DatabaseContext>().ToMethod(x => ApplicationContext.Current.DatabaseContext);//.InRequestScope();
                Bind<IHealthChecks>().ToMethod(x => UmbracoConfig.For.HealthCheck());//.InRequestScope();
            }
        }
    }
}