﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using Ninject;
using Ninject.Modules;

namespace MyUmbraco.Application.IoC
{
    /// <summary>
    /// Its job is to Register Ninject Modules and Resolve Dependencies for both MVC and Web API 2
    /// </summary>
    public class NinjectContainer
    {
        private static NinjectResolver _resolver;

        //Register Ninject Modules
        public static void RegisterModules(NinjectModule[] modules)
        {
            _resolver = new NinjectResolver(modules);
            
            //Hookup MVC Pipeline
            System.Web.Mvc.DependencyResolver.SetResolver(_resolver);

            //Web API Pipeline
            GlobalConfiguration.Configuration.DependencyResolver = _resolver;
        }

        //Manually Resolve Dependencies
        public static T Resolve<T>()
        {
            return _resolver.Kernel.Get<T>();
        }
    }
}