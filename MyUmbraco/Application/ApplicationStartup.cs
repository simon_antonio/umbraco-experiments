﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using MyUmbraco.Application.Controllers.Core;
using MyUmbraco.Application.IoC;
using Our.Umbraco.DocTypeGridEditor.Web.Mvc;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace MyUmbraco.Application
{
    public class ApplicationStartup : IApplicationEventHandler
    {
        public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            Config.SetJsonFormatter(GlobalConfiguration.Configuration);
            NinjectContainer.RegisterModules(NinjectHttpModules.Modules);
            //todo - do we need this?
            //DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
        }

        public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {

        }

        public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            DefaultRenderMvcControllerResolver.Current.SetDefaultControllerType(typeof(DefaultController));

            if (!DefaultDocTypeGridEditorSurfaceControllerResolver.HasCurrent)
            {
                DefaultDocTypeGridEditorSurfaceControllerResolver.Current = new DefaultDocTypeGridEditorSurfaceControllerResolver();
            }

            DefaultDocTypeGridEditorSurfaceControllerResolver.Current.SetDefaultControllerType(typeof(WidgetSurfaceController));
        }
    }
}