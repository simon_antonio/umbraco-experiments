﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using MyUmbraco.Application.Services.Interfaces;

namespace MyUmbraco.Application.Services
{
    public class SiteConfiguration : ISiteConfiguration
    {
        public int ItemsPerPage => Convert.ToInt32(ConfigurationManager.AppSettings["ItemsPerPage"]);
        public string[] DeveloperIpAddresses => ConfigurationManager.AppSettings["DeveloperIpAddresses"]?.Replace(" ","").Split(',');
        public bool IsDebugging => HttpContext.Current.IsDebuggingEnabled;

        public bool IsDeveloper(HttpContext context)
        {
            return context != null && (DeveloperIpAddresses?.Contains(context.Request.UserHostAddress) ?? false);
        }
    }
}