﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyUmbraco.Application.Services.Interfaces;

namespace MyUmbraco.Application.Services
{
    public class CacheResolver : ICacheResolver
    {
        public string GlobalCacheKey { get; } = typeof(ApplicationStartup).Namespace;
    }
}