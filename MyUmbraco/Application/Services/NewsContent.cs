﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyUmbraco.Application.Services.Interfaces;
using Umbraco.Web;

namespace MyUmbraco.Application.Services
{
    public class NewsContent : INewsContext
    {
        public List<object> GetLatestNews()
        {
            var helper = new UmbracoHelper(UmbracoContext.Current);
            var t =helper.TypedContent(2087);
            return new List<object>()
            {
                new {Message = "abc"},
                new {Message = "def"},
            };
        }
    }
}