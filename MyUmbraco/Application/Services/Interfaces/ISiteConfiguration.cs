﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyUmbraco.Application.Services.Interfaces
{
    /// <summary>
    /// Mapping to some basic site constants
    /// </summary>
    public interface ISiteConfiguration
    {
        int ItemsPerPage { get; }
        string[] DeveloperIpAddresses { get; }
        bool IsDebugging { get; }
        bool IsDeveloper(HttpContext context);
    }
}