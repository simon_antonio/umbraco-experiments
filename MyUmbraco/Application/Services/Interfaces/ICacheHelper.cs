﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyUmbraco.Application.Services.Interfaces
{
    public interface ICacheResolver
    {
       string GlobalCacheKey { get; }

        //todo work out how to regiester dependancies for cache clearing
    }
}