﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyUmbraco.Application.Services.Interfaces
{
    public interface INewsContext
    {
        List<object> GetLatestNews();
    }
}