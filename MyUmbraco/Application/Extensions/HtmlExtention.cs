﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace MyUmbraco.Application.Extensions
{
    public static class HtmlExtention
    {
        public static HtmlString OptionalMetaTag(this HtmlHelper helper, string attributeName, string attributeValue, object value, int? cropWidth = null, int? cropHeight = null)
        {
            if (string.IsNullOrWhiteSpace(value?.ToString())) return new HtmlString(string.Empty);

            var url = value.ToString();

            if (value is IPublishedContent content)
            {
                url = content.Url;
                if (content.DocumentTypeAlias.Equals("image"))
                {
                    url = url.GetCropUrl(width: cropWidth, height: cropHeight);
                }
            }

            return new HtmlString($"<meta {attributeName}=\"{attributeValue}\" content=\"{url}\">");
        }
    }
}