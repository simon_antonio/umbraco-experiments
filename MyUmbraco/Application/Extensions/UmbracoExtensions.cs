﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace MyUmbraco.Application.Extensions
{
    public static class UmbracoExtensions
    {
        public static IMedia CreateMedia(this IMediaService ms, int parentId, HttpPostedFile file)
        {
            var mediaTypeAlias = "File";
            switch( file.ContentType )
            {
                case "image/jpg":
                case "image/jpeg":
                case "image/png":
                case "image/gif":
                    mediaTypeAlias = "Image";
                    break;
            }

            var newImage = ms.CreateMedia(file.FileName, parentId, mediaTypeAlias);
            newImage.SetValue("umbracoFile", file);
            ms.Save(newImage);

            return newImage;
        }

        /// <summary>
        /// Determines if a node should be shown (in nav)
        /// </summary>
        /// <returns>True if node should be shown</returns>
        public static bool IsShownInMenu(this IPublishedContent node)
        {
            if (node.HasProperty("umbracoNaviHide") && node.HasValue("umbracoNaviHide") && node.GetPropertyValue<bool>("umbracoNaviHide"))
            {
                return false;
            }

            switch (node.DocumentTypeAlias)
            {
                //any doctype's that should now be shown in nav
                case "blog":
                case "newsArticle":
                case "blogPost":
                case "event":
                    return false;
            }

            return node.IsVisible();
        }

        /// <summary>
        /// Determines if a node should be shown (in nav)
        /// </summary>
        /// <returns>True if node should be shown</returns>
        public static IEnumerable<IPublishedContent> VisibileChildren(this IPublishedContent node)
        {
            return node.Children.Where(x=>x.IsShownInMenu());
        }

        ///// <summary>
        ///// Return all fields required for paging.
        ///// </summary>
        ///// <param name="itemsPerPage"></param>
        ///// <param name="numberOfItems"></param>
        ///// <returns></returns>
        //public static Pager GetPager(this UmbracoHelper umbraco, int itemsPerPage, int numberOfItems)
        //{
        //    // paging calculations
        //    int currentPage;
        //    if (!int.TryParse(HttpContext.Current.Request.QueryString["page"], out currentPage))
        //    {
        //        currentPage = 1;
        //    }

        //    var numberOfPages = numberOfItems % itemsPerPage == 0 ? Math.Ceiling((decimal)(numberOfItems / itemsPerPage)) : Math.Ceiling((decimal)(numberOfItems / itemsPerPage)) + 1;
        //    var pages = Enumerable.Range(1, (int)numberOfPages);

        //    return new Pager()
        //    {
        //        NumberOfItems = numberOfItems,
        //        ItemsPerPage = itemsPerPage,
        //        CurrentPage = currentPage,
        //        Pages = pages
        //    };
        //}

        public static bool IsBackoffice(this UmbracoHelper helper)
        {
            if (HttpContext.Current == null) return true;

            if (HttpContext.Current.Request.Url.AbsolutePath.StartsWith("/umbraco")) return true;

            try
            {
                var test = helper.AssignedContentItem;
            }
            catch
            {
                //if we call AssignedContentItem from the back office we throw an exception. (stupid umbraco)
                return true;
            }

            return false;
        }
    }
}