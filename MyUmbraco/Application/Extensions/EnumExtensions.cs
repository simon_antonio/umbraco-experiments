﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;

namespace MyUmbraco.Application.Extensions
{
    public static class EnumExtensions
    {
        public static string GetGroupName(this Enum value)
        {
            try
            {
                return value
                .GetType()
                .GetField(value.ToString())
                .GetCustomAttributes<DisplayAttribute>()
                .Select(a => a.GetGroupName())
                .FirstOrDefault() ?? value.ToString();
            }
            catch (Exception)
            {
                if (value != null)
                {
                    return value.ToString();
                }
                return string.Empty;
            }
        }

        public static T GetValueFromName<T>(string name)
        {
            var type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException();

            foreach (var field in type.GetFields())
            {
                if (Attribute.GetCustomAttribute(field,
                    typeof(DisplayAttribute)) is DisplayAttribute attribute)
                {
                    if (attribute.Name == name || attribute.GroupName == name)
                    {
                        return (T)field.GetValue(null);
                    }
                }
                else
                {
                    if (field.Name == name)
                        return (T)field.GetValue(null);
                }
            }

            throw new ArgumentOutOfRangeException(nameof(name));
        }
    }
}