﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using MyUmbraco.Application.Controllers.Core;
using Our.Umbraco.DocTypeGridEditor.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace MyUmbraco.Application.Extensions
{
    public static class StaticGridContentHelperExentions
    {
        public static HtmlString RenderStaticContentItem(this HtmlHelper helper, string viewName, string controllerName = null, string nodeId = null)
        {
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);

            IPublishedContent content = null;
            if (nodeId != null)
            {
                content = umbracoHelper.TypedContent(nodeId);
            }

            try
            {
                //accessing this property when we don't have a proper front office context will cause an exception
                if (umbracoHelper.AssignedContentItem == null)
                {
                   
                }
            }
            catch
            {
                if (content == null)
                {
                    return new HtmlString(string.Empty);
                }
            }

            if (content == null)
            {
                content = umbracoHelper.AssignedContentItem;
            }

            //where controller is specified in grid.editors.config.js
            if (!string.IsNullOrEmpty(controllerName))
            {
                var controllerSurfaceName = controllerName + "Surface";
                var actionName = viewName;

                if (umbracoHelper.SurfaceControllerExists(controllerSurfaceName, actionName, true))
                {
                    return helper.Action(actionName, controllerSurfaceName, new DoctypeGridEditorModel()
                    {
                        dtgeModel = content,
                        dtgeViewPath = viewName,
                        dtgePreviewViewPath = ""
                    });
                }
            }
            else
            {
                //check if we have any basic controllers matching AliasSurfaceController + ActionName.
                var controllerSurfaceName = viewName + "Surface";
                var actionName = viewName;

                if (umbracoHelper.SurfaceControllerExists(controllerSurfaceName, actionName, true))
                {
                    return helper.Action(actionName, controllerSurfaceName, new DoctypeGridEditorModel()
                    {
                        dtgeModel = content,
                        dtgeViewPath = viewName,
                        dtgePreviewViewPath = ""
                    });
                }
            }



            var defaultController = DefaultDocTypeGridEditorSurfaceControllerResolver.Current.GetDefaultControllerType();
            var defaultControllerName = defaultController.Name.Substring(0, defaultController.Name.LastIndexOf("Controller", StringComparison.Ordinal));

            // Just go with a default action name
            return helper.Action("Index", defaultControllerName, new DoctypeGridEditorModel()
            {
                dtgeModel = content,
                dtgeViewPath = viewName,
                dtgePreviewViewPath = ""
            });
        }

        public static bool SurfaceControllerExists(this UmbracoHelper helper, string controllerName, string actionName = "Index")
        {
            // Setup dummy route data
            var rd = new RouteData();
            rd.DataTokens.Add("area", "umbraco");
            rd.DataTokens.Add("umbraco", "true");

            // Setup dummy request context
            var rc = new RequestContext(
                new HttpContextWrapper(HttpContext.Current),
                rd);

            // Get controller factory
            var cf = ControllerBuilder.Current.GetControllerFactory();

            // Try and create the controller
            try
            {
                var ctrl = cf.CreateController(rc, controllerName);

                if (!(ctrl is SurfaceController ctrlInstance))
                    return false;

                foreach (var method in ctrlInstance.GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance)
                    .Where(x => typeof(ActionResult).IsAssignableFrom(x.ReturnType)))
                {
                    if (method.Name == actionName)
                    {
                        return true;
                    }

                    var attr = method.GetCustomAttribute<ActionNameAttribute>();
                    if (attr != null && attr.Name == actionName)
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool SurfaceControllerExists(this UmbracoHelper helper, string name, string actionName = "Index", bool cacheResult = true)
        {
            if (!cacheResult)
                return SurfaceControllerExists(helper, name, actionName);

            return (bool)ApplicationContext.Current.ApplicationCache.RuntimeCache.GetCacheItem(
                string.Join("_", new[] { "Our.Umbraco.Mortar.Web.Extensions.UmbracoHelperExtensions.SurfaceControllerExists", name, actionName }),
                () => SurfaceControllerExists(helper, name, actionName));
        }
    }
}