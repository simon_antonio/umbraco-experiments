﻿using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace MyUmbraco.Application.Models.Umbraco
{
    public partial class SiblingPageList 
    {
        public IEnumerable<Base> FilteredSiblings { get; set; }
    }
}