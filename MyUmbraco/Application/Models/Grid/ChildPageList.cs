﻿using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace MyUmbraco.Application.Models.Umbraco
{
    public partial class ChildPageList 
    {
        public IEnumerable<Base> FilteredChildren { get; set; }
    }
}