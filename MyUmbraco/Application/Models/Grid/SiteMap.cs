﻿using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;

namespace MyUmbraco.Application.Models.Grid
{
    /// <summary>
    /// For use with SiteMap static grid item & SiteMapSurfaceController
    /// </summary>
    public class SiteMapItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<SiteMapItem> Children { get; set; }

        public string Url { get; set; }
        public string DocumentTypeAlias { get; set; }

        public SiteMapItem(IPublishedContent node)
        {
            Id = node.Id;
            Name = node.Name;
            Children = node.Children?.Any() ?? false ? new List<SiteMapItem>() : null;
            Url = node.Url;
            DocumentTypeAlias = node.DocumentTypeAlias;
        }
    }
}