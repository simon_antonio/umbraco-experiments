﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web;

// ReSharper disable once CheckNamespace
namespace MyUmbraco.Application.Models.Umbraco
{
    public partial class Base
    {
        /// <summary>
        /// Return the title of the page. Will use Alt Title (Meta Tab) if set, otherwise falls back to Node Name
        /// </summary>
        public string Title
        {
            get
            {
                if (_title == null)
                {
                    var seo = this.OfType<IMeta>();
                    _title = string.IsNullOrEmpty(seo?.AltTitle) ? Content.Name : seo.AltTitle;
                }

                return _title;
            }
        }

        private string _title;//lazy loaded

        /// <summary>
        /// Get the schema type for a html page
        /// </summary>
        public string SchemaType
        {
            get
            {
                if (_schemaType == null)
                {
                    var social = this.OfType<IMeta>();
                    _schemaType = !string.IsNullOrEmpty(social?.SchemaType)
                        ? $"itemscope itemtype=\"{social.SchemaType.Trim()}\""
                        : string.Empty;
                }

                return _schemaType;//lazy loaded
            }
        }

        private string _schemaType;
    }
}