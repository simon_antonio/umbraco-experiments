﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyUmbraco.Application.Controllers.Core;
using MyUmbraco.Application.Models.Umbraco;
using MyUmbraco.Application.Services.Interfaces;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace MyUmbraco.Application.Controllers.Template
{
    public class NewsSectionController : SurfaceRenderMvcController
    {
        private readonly INewsContext _newsContext;
        private readonly ApplicationContext _context;
        private readonly DatabaseContext _database;

        public NewsSectionController(INewsContext newsContext, ApplicationContext context, DatabaseContext database)
        {
            _newsContext = newsContext;
            _context = context;
            _database = database;
        }

        public override ActionResult Index(RenderModel model)
        {
            // get the current template name
            var template = this.ControllerContext.RouteData.Values["action"].ToString();

            var test = Umbraco.AssignedContentItem;
            var test2 = ApplicationContext.Services.ContentService.GetById(2087);
            var test3 = Umbraco.TypedContent(2087);

            var anotherTest = _context.IsConfigured;
            var t3 = _database.ConnectionString;

            // return the view with the model as the id of the custom class
            return View(template, model.Content);
        }
    }
}