﻿using System;
using System.Web.Mvc;
using MyUmbraco.Application.Extensions;
using Our.Umbraco.DocTypeGridEditor.Web.Controllers;
using Umbraco.Core.Logging;

namespace MyUmbraco.Application.Controllers.Core
{
    /// <summary>
    /// All grid item controllers should inherit from this class
    /// Handles static and doctypegrid widgets
    /// </summary>
    public class WidgetSurfaceController : DocTypeGridEditorSurfaceController
    {
        /// <summary>
        /// If the route hijacking doesn't find a controller this default controller will be used.
        /// That way a each page will always go through a controller and we can always have a BaserModel for the masterpage.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(DoctypeGridEditorModel model = null)
        {
            return !string.IsNullOrEmpty(model?.dtgeViewPath) ? CurrentPartialView(model) : CurrentPartialView(new DoctypeGridEditorModel(){dtgeModel = this.Model});
        }

        //pass in an arbitary model, we need to tell DTGE what the ViewPath is (ie ~/Views/Grid/your-widget.csthml where this.ViewPath is your-widget)
        protected new PartialViewResult CurrentPartialView(object model = null)
        {
            return CurrentPartialView(new DoctypeGridEditorModel()
            {
                dtgeModel = model,
                dtgeViewPath = this.ViewPath
            });
        }

        protected PartialViewResult CurrentPartialView(DoctypeGridEditorModel model = null)
        {
            //rework all of this.
            try
            {
                //TODO - fixme
                if (Request.RequestContext.RouteData.Values["action"].ToString().ToLower() == "json")
                {
                    const string viewName = "~/Views/Grid/JSON.cshtml";
                    return PartialView(viewName, model?.dtgeModel);
                }

                if (MightBeStaticGrid(ViewPath))
                {
                    return PartialView($"~/Views/Grid/{model?.dtgeViewPath.UppercaseFirst()}.cshtml", model?.dtgeModel);
                }

                return PartialView($"~{ViewPath}{Model.DocumentTypeAlias.UppercaseFirst()}.cshtml", model?.dtgeModel);
            }
            catch (Exception ex)
            {
                LogHelper.Error(GetType(), "Error in view", ex);
                throw;
            }
        }

        //static widgets have a short view path. It may return false positives or negatives when using this surface controller for other logic
        private bool MightBeStaticGrid(string viewPath)
        {
            if (!viewPath.Contains("/Views/Grid/"))
            {
                return true;
            }

            return false;
        }
    }
}