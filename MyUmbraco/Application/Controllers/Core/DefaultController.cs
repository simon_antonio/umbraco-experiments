﻿using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace MyUmbraco.Application.Controllers.Core
{
    public class DefaultController: SurfaceRenderMvcController
    {
        /// <inheritdoc />
        /// <summary>
        /// If the route hijacking doesn't find a controller this default controller will be used.
        /// That way a each page will always go through a controller and we can always have a BaserModel for the masterpage.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override ActionResult Index(RenderModel model)
        {
            return CurrentTemplate(model);
        }

        public ActionResult RenderGrid()
        {
            dynamic gridContent = CurrentPage.GetPropertyValue<dynamic>("bodyContent", false, null);
            // ReSharper disable once Mvc.PartialViewNotResolved
            return PartialView("Grid/SiteGridRenderer", gridContent);
        }
    }
}