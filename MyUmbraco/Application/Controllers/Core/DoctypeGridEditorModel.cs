﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyUmbraco.Application.Controllers.Core
{
    /// <summary>
    /// Model for passing dtge settings into widget surface controller
    /// Typically from StaticGrid Content Helper
    /// </summary>
    public class DoctypeGridEditorModel
    {
        // ReSharper disable once InconsistentNaming
        public object dtgeModel { get; set; }
        // ReSharper disable once InconsistentNaming
        public string dtgeViewPath { get; set; }
        // ReSharper disable once InconsistentNaming
        public string dtgePreviewViewPath { get; set; }
    }
}