﻿using System;
using System.IO;
using System.Web;
using Umbraco.Core;
using Umbraco.Web;
using System.Web.Mvc;
using MyUmbraco.Application.Extensions;
using Umbraco.Core.Configuration;
using Umbraco.Web.Routing;
using Umbraco.Web.Security;

namespace MyUmbraco.Application.Controllers.Core
{
    /// <summary>
    /// Used for back office preview of Static Grid Items
    /// url: /umbraco/surface/StaticGridView/GetView/?view={alias}&nodeId={nodeId}
    /// </summary>
    public class StaticGridViewController : WidgetSurfaceController
    {
        public HtmlString GetView(string view, string controller = null)
        {
            if (Request["controller"] != null)
            {
                controller = Request["controller"];
            }

            string nodeId = null;
            if (Request["nodeId"] != null)
            {
                nodeId = Request["nodeId"];
            }

            var htmlHelper = CreateHtmlHelper();
            return htmlHelper.RenderStaticContentItem(view, controller, nodeId);
        }

        private static HtmlHelper CreateHtmlHelper()
        {
            var dummyHttpContext = new HttpContextWrapper(System.Web.HttpContext.Current);
            UmbracoContext.EnsureContext(
                dummyHttpContext,
                ApplicationContext.Current,
                new WebSecurity(dummyHttpContext, ApplicationContext.Current),
                UmbracoConfig.For.UmbracoSettings(),
                UrlProviderResolver.Current.Providers,
                false);

            var cc = new ControllerContext
            {
                RequestContext = UmbracoContext.Current.HttpContext.Request.RequestContext
            };
            var viewContext = new ViewContext(cc, new FakeView(), new ViewDataDictionary(), new TempDataDictionary(), new StringWriter());
            var htmlHelper = new HtmlHelper(viewContext, new ViewPage());
            return htmlHelper;
        }

        private class FakeView : IView
        {
            public void Render(ViewContext viewContext, TextWriter writer)
            {
            }
        }
    }
}