﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyUmbraco.Application.Controllers.Core;
using MyUmbraco.Application.Extensions;
using MyUmbraco.Application.Models.Grid;
using MyUmbraco.Application.Services.Interfaces;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace MyUmbraco.Application.Controllers.Grid
{
    public class SiteMapSurfaceController : WidgetSurfaceController
    {
        private readonly ICacheResolver _cacheResolver;

        public SiteMapSurfaceController(ICacheResolver cacheResolver)
        {
            _cacheResolver = cacheResolver;
        }
        public ActionResult SiteMap()
        {
            if (Umbraco.IsBackoffice()) return CurrentPartialView(new SiteMapItem(Umbraco.TypedContent(2087)));

            //cached as rebuilding this tree can be very time consuming if there are lots of nodes.
            return ApplicationContext.ApplicationCache.RuntimeCache
                .GetCacheItem($"{_cacheResolver.GlobalCacheKey}-SiteMap",
                    () =>
                    {
                        //maybe move to service?
                        var root = Umbraco.AssignedContentItem.Site();
                        var mapRoot = ProcessNode(root);

                        return CurrentPartialView(mapRoot);
                    }) as ActionResult;
        }

        private SiteMapItem ProcessNode(IPublishedContent node)
        {
            var mapItem = new SiteMapItem(node);
            foreach (var child in node.Children)
            {
                mapItem.Children.Add(ProcessNode(child));
            }
            return mapItem;
        }
    }
}