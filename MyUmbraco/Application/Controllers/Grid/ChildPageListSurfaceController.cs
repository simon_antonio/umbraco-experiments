﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyUmbraco.Application.Controllers.Core;
using MyUmbraco.Application.Extensions;
using MyUmbraco.Application.Models.Grid;
using MyUmbraco.Application.Models.Umbraco;
using MyUmbraco.Application.Services.Interfaces;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace MyUmbraco.Application.Controllers.Grid
{
    public class ChildPageListSurfaceController : WidgetSurfaceController
    {
        private readonly ICacheResolver _cacheResolver;

        public ChildPageListSurfaceController(ICacheResolver cacheResolver)
        {
            _cacheResolver = cacheResolver;
        }

        public ActionResult ChildPageList()
        {
            var expectedModel = new ChildPageList(Model);

            if (Umbraco.IsBackoffice()) return CurrentPartialView(expectedModel);

            //filter based upon grid item settings

            //maybe move to a service?
            var children = Umbraco.AssignedContentItem.Children;
            if (!string.IsNullOrEmpty(expectedModel.DocumentTypeFilter))
            {
                children = children.Where(x =>
                    expectedModel.DocumentTypeFilter.ToLower().Split(',').Contains(x.DocumentTypeAlias.ToLower()));
            }

            if (expectedModel.SortType != null)
            {
                switch (expectedModel.SortType)
                {
                    case "Create":
                        children = children.OrderByDescending(x => x.CreateDate);
                        break;
                    case "Alphabetical":
                        children = children.OrderBy(x => x.Name);
                        break;
                    case "Random":
                        children = children.Shuffle();
                        break;
                }
            }

            if (expectedModel.NumberOfPages > 0)
            {
                children = children.Take(expectedModel.NumberOfPages);
            }

            expectedModel.FilteredChildren = children.OfType<Base>();

            return CurrentPartialView(expectedModel);
        }

    }
}