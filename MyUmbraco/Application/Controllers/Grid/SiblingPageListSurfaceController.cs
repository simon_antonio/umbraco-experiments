﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyUmbraco.Application.Controllers.Core;
using MyUmbraco.Application.Extensions;
using MyUmbraco.Application.Models.Grid;
using MyUmbraco.Application.Models.Umbraco;
using MyUmbraco.Application.Services.Interfaces;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace MyUmbraco.Application.Controllers.Grid
{
    public class SiblingPageListSurfaceController : WidgetSurfaceController
    {
        private readonly ICacheResolver _cacheResolver;

        public SiblingPageListSurfaceController(ICacheResolver cacheResolver)
        {
            _cacheResolver = cacheResolver;
        }

        public ActionResult SiblingPageList()
        {
            var expectedModel = new SiblingPageList(Model);

            if (Umbraco.IsBackoffice()) return CurrentPartialView(expectedModel);

            //filter based upon grid item settings

            //maybe move to a service?
            var siblings = Umbraco.AssignedContentItem.Siblings();
            if (!string.IsNullOrEmpty(expectedModel.DocumentTypeFilter))
            {
                siblings = siblings.Where(x =>
                    expectedModel.DocumentTypeFilter.ToLower().Split(',').Contains(x.DocumentTypeAlias.ToLower()));
            }

            if (expectedModel.SortType != null)
            {
                switch (expectedModel.SortType)
                {
                    case "Create":
                        siblings = siblings.OrderByDescending(x => x.CreateDate);
                        break;
                    case "Alphabetical":
                        siblings = siblings.OrderBy(x => x.Name);
                        break;
                    case "Random":
                        siblings = siblings.Shuffle();
                        break;
                }
            }

            if (expectedModel.NumberOfPages > 0)
            {
                siblings = siblings.Take(expectedModel.NumberOfPages);
            }

            expectedModel.FilteredSiblings = siblings.OfType<Base>();

            return CurrentPartialView(expectedModel);
        }

    }
}