﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyUmbraco.Application
{
    /// <summary>
    /// Used in Global.ascx
    /// </summary>
    public class Global : Umbraco.Web.UmbracoApplication
    {
        public override string GetVaryByCustomString(HttpContext context, string arg)
        {
            if (arg == "rawUrl")
            {
                return "rawUrl=" + context.Request.Url;
            }

            return base.GetVaryByCustomString(context, arg);
        }
    }
}