﻿angular.module('umbraco').controller('StaticGridContentController', function ($scope, $http, editorState) {

    $scope.preview = 'Loading...';
    getEditorMarkupForDocTypePartial($scope.control.editor.config.viewName, $scope.control.editor.config.controllerName, $scope.control.editor.config.site).success(function (htmlResult) {
        if (htmlResult.trim().length > 0) {
            $scope.preview = htmlResult;
        }
    });


    function getEditorMarkupForDocTypePartial(view, controllerName, site) {
        var url = '/umbraco/surface/StaticGridView/GetView/?view=' + view + '&nodeId=' + editorState.getCurrent().id;
        if (controllerName != null) {
            url += '&controller=' + controllerName;
        }
        if (site != null) {
            url += '&site=' + site;
        }

        return $http({
            method: 'GET',
            url: url
        });
    }

});