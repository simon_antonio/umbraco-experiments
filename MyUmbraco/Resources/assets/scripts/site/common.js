﻿import * as $ from 'jquery';

var App = App || {};
App.Common = (function () {

    var init = function () {
        console.log('initializing common');
    };


    /*------------------------------------------------------------------------
    * Expose publics
    ------------------------------------------------------------------------*/
    return {
        init: init
    };

})();

export default App;