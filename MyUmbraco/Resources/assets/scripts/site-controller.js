﻿import { $,jQuery } from 'jquery';
import App from './site/common.js';
import css from '../styles/main.scss';

//export jquery
window.$ = $;
window.jQuery = jQuery;

(function ($) {

    App.Common.init();

    var loadClass = $('body').data('jsload');

    if (App[loadClass] != undefined && App[loadClass].init != undefined) {
        App[loadClass].init();
    }
})(jQuery);